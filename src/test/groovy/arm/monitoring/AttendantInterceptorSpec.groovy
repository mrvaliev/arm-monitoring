package arm.monitoring


import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(AttendantInterceptor)
class AttendantInterceptorSpec extends Specification {

    def setup() {
    }

    def cleanup() {

    }

    void "Test attendant interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"attendant")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
