package arm.monitoring

import arm.monitoring.security.Role
import arm.monitoring.security.User
import arm.monitoring.security.UserRole
import grails.transaction.Transactional

@Transactional
class InitDataService {

    def fillDatabase() {

        def roleCam = new Role(authority: "SPECIALIST_CAM", name: "Cпец. ЦАМ").save(flush: true)
        def adminRole = new Role(authority: "ADMIN", name: "Cпец. Админ").save(flush: true)

        def depGroup1 = new DepartmentGroup(name: "ЦДНГ")
        def depGroup2 = new DepartmentGroup(name: "ЦПРС")
        def depGroup3 = new DepartmentGroup(name: "ЦППД")
        def depGroup4 = new DepartmentGroup(name: "ЦКНН")
        def dep1 = new Department(name: "ЦДНГ-1 (тест)", group: depGroup1).save();
        def dep2 = new Department(name: "ЦПРС-1 (тест)", group: depGroup2).save();
        def dep3 = new Department(name: "ЦППД-2 (тест)", group: depGroup3).save();
        def dep4 = new Department(name: "ЦКНН-2 (тест)", group: depGroup4).save();

        def empl = new Employee(department: dep1, fio: "Василий Иванов").save()
        def emp2 = new Employee(department: dep1, fio: "Наиль Галиев").save()


        def user = new User(
                username: "admin",
                fio: "Админ 1",
                password: "123",
                department: dep1,
                email: "admin@mail.ru").save(flush: true);

        def userCam = new User(
                username: "spcam",
                fio: "тестовый пользователь 2 ",
                password: "123",
                department: dep2,
                email: "user2@mail.ru").save(flush: true);

        UserRole.create(user, adminRole)
        UserRole.create(userCam, roleCam)
    }

    def fillIssues() {
        List<String[]> lines = loadFromCsv("kontr_karta.csv")

        IssueGroup issueGroup;

        lines.each { line ->
            if (line.size() == 1) {
                if (issueGroup) issueGroup.save();
                issueGroup = new IssueGroup(name: line[0])
            } else {
                Issue issue = new Issue(id: line[0], name: line[1])
                if (issueGroup) {
                    issueGroup.addToIssues(issue)
                }
            }
        }
    }

    def createTestIncidents(int count) {

        def detectTime = new Date()
        def user = User.get(1)
        def emp = Employee.get(1)
        count.times {
            Issue issue = Issue.get(it.intValue() + 1);
            Department department = Department.get((it.intValue() % 4) + 1)
            String descr = "описание обсуждения проблемы. краткое"
            new Incident(issue: issue,
                    department: department,
                    issueTime: detectTime,
                    descr: "замечение",
                    detectTime: detectTime,
                    employee: emp,
                    solvingDescr: descr,
                    note: "доп. информация",
                    createdBy: user,
                    modifiedBy: user,
                    levelTime: detectTime).save()
        }
    }

    List<String[]> loadFromCsv(String file) {
        List<String[]> lines = new ArrayList<>();
        InputStream resource = this.class.classLoader.getResourceAsStream("kontr_karta.csv");
        def csv = new InputStreamReader(resource);
        csv.splitEachLine(";", { line -> lines.add(line) })
        return lines;
    }
}
