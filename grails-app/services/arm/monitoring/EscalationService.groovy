package arm.monitoring

import grails.transaction.Transactional

@Transactional
class EscalationService {

    def initalEscalation() {
        def list = Incident.findAll("from Incident where escalation=null");
        list.each { it ->
            it.escalation = new Escalation(dateTime: new Date(), level: EscalationLevel.FIRST)
            it.save();
        }
    }
}
