package arm.monitoring

import arm.monitoring.security.User
import arm.monitoring.security.UserRole
import grails.transaction.Transactional
import org.springframework.security.core.userdetails.UserDetails

@Transactional
class UserInfoService {


    def springSecurityService

    User getCurrentUser() {
        UserDetails auth = (UserDetails) springSecurityService.getPrincipal()
        String username = auth.getUsername()
        UserRole user = UserRole.find("from UserRole where user.username=:name", [name: username])
        return user?.user;
    }
}
