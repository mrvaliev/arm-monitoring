<div id="confirm" class="modal hide fade">
    <div class="modal-body">
        Вы уверены ?
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Удалить</button>
        <button type="button" data-dismiss="modal" class="btn">Отмена</button>
    </div>
</div>