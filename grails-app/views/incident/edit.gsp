<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'incident.label', default: 'Incident')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-incident" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="edit-incident" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.incident}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.incident}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.incident}" method="PUT">
        <g:hiddenField name="version" value="${this.incident?.version}"/>
        <fieldset class="form col-sm-6 form-group" style="background-color: #ebffa7">


            <my:select name="employee.id" label="Работник" prop="employee"
                       optionKey="id"
                       optionValue="fio"
                       from="${arm.monitoring.Employee.list()}">
            </my:select>

            <g:select label="Проблема" name="issue.id" value="${incident.issue?.id}" class="form-control"
                      from="${arm.monitoring.Issue.list()}"
                      optionKey="id"
                      optionValue="name"/>

            <my:field bean="${incident}" prop="descr" label="Комментарий"></my:field>

            <my:datePicker label="Время возникновения" name="issueTime" value="${new Date()}"
                           precision="hour"
                           years="${2017..2020}"/>


            <my:datePicker label="Время выявления" name="detectTime" value="${new Date()}"
                           precision="hour" prop=""
                           years="${2017..2020}"/>

            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
