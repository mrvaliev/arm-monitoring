<%@ page import="arm.monitoring.Issue; arm.monitoring.Department" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'incident.label', default: 'Incident')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-incident" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.incident}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.incident}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.incident}" method="POST">
        <fieldset class="form col-sm-6 form-group" style="background-color: #ebffa7">

            <my:select id="employeeSelect"
                       name="employee.id" label="Работник" prop="employee"
                       value="${emp ? emp.id : null}"
                       optionKey="id"
                       optionValue="fio"
                       noSelection="${['null': 'Выберите...']}"
                       from="${arm.monitoring.Employee.list()}">

            </my:select>

            <my:select id="issueGroupSelect" name="issueGroup.id"
                       value="${ig ? ig.id : null}"
                       label="Группа проблемы" from="${arm.monitoring.IssueGroup.list()}"
                       class="form-control"
                       noSelection="${['': 'Выберите...']}"
                       optionKey="id" optionValue="name"/>

            <my:select id="issueSelect"
                       label="Проблема" name="issue.id" value="${incident.issue?.id}" class="form-control"
                       from="${issuesList}"
                       optionKey="id"
                       noSelection="${['null': 'Выберите...']}"
                       optionValue="name"/>



            <my:field bean="${incident}" prop="descr" label="Комментарий"></my:field>

            <my:datePicker label="Время возникновения" name="issueTime" value="${new Date()}"
                           precision="hour"
                           years="${2017..2020}"/>


            <my:datePicker label="Время выявления" name="detectTime" value="${new Date()}"
                           precision="hour" prop=""
                           years="${2017..2020}"/>

            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
