<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'incident.label', default: 'Incident')}"/>
    <title>Список инцидентов</title>
</head>

<body>
<g:render template="/shared/mainMenu"></g:render>

<div class="nav" role="navigation">
    <ul>
        %{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
        <li><g:link class="create" action="create">Новый инцидент</g:link></li>
        <li><a data-toggle="modal" data-target="#myModal" id="stesc" href="/start-escalation">
            <span class="glyphicon glyphicon-play"></span> Запуск эскалации</a>
        </li>
        <li class="pull-right"><div class="pagination pagination-custom">
            <g:paginate total="${incidentCount ?: 0}"/></div>
        </li>
    </ul>
</div>

<div id="list-incident" class="scaffold-list table-responsive">

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="alert-popup" role="alert"></div>
    <table class="table table-bordered table-hover">
        <thead>
        %{--<th>#</th>--}%
        %{--<th>Группа нарушения</th>--}%
        <th>Цех</th>
        <th>ФИО <br/> работника</th>
        <th>Выявленная проблема</th>
        <th>Заметка</th>
        <th>Дата <br/> нарушения</th>
        <th>Время <br/> выявления</th>
        <th>Состояние</th>
        <th>Время <br/> эскалации <br/> на тек. уровень</th>
        <th>Время <br/> на решение <br/> (час)</th>
        <th>Текущий <br/> уровень</th>
        <th>Дата завершения <br/> ДК(проекта)</th>
        %{--<th>Описание решения</th>--}%
        <th>Создатель</th>
        <th class="actions-cell"><b>Действия</b></th>
        </thead>
        <tbody>
        <g:each in="${incidentList}" var="inc">
            <tr>
                <td>${inc.employee.department.name}</td>
                <td><g:link controller="employee" action="incidents" title="Просмотр инцидентов сотрудника"
                            params="[empID: inc.employee.id, date: curDate]">
                    ${inc.employee.fio}</g:link></td>
                <td>${inc.issue.name}</td>
                <td>${inc.descr}</td>
                <td><g:formatDate format="yyyy-MM-dd" date="${inc.issueTime}"/></td>
                <td><g:formatDate format="yyyy-MM-dd" date="${inc.detectTime}"/></td>
                <g:if test="${inc.solved}">
                    <td class="success">Решено</td>
                </g:if>
                <g:else>
                    <td class="danger">не решено</td>
                </g:else>
                <td>
                    <g:if test="${inc.escalation != null}">
                        <g:formatDate format="yyyy-MM-dd HH:mm" date="${inc.escalation?.dateTime}"/>
                    </g:if>
                    <g:else>
                        Нет
                    </g:else>
                </td>
                <td>${inc.getSolvingTime()}</td>

                <td>${inc.getEscalation()?.getLevel()?.getLabel()}</td>
                <td>${inc.issue.roadMap?.endDate}</td>
                %{--<td>${inc.solvingDescr}</td>--}%
                <td>${inc.createdBy.fio}</td>
                <sec:ifAnyGranted roles="SPECIALIST_NGDU,SPECIALIST_CEH">
                    <td class="actions-cell">
                        <g:link action="report" id="${inc.id}"><span title="Написать отчет"
                                                                     class="glyphicon glyphicon-ok"></span></g:link>
                    </td>
                </sec:ifAnyGranted>
                <sec:ifAnyGranted roles="ADMIN,SPECIALIST_CAM">
                    <td class="actions-cell"><g:link action="edit" id="${inc.id}"><span title="Изменить"
                                                                                        class="glyphicon glyphicon-edit"></span></g:link>
                        <br/>
                        <a href="#" data-toggle=confirmation
                           data-href="${request.contextPath}/incident/delete/${inc.id}">
                            <span title="Удалить" class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </sec:ifAnyGranted>

            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Запуск экскалации</h4>
            </div>

            <div class="modal-body">
                Запустить эскалацию для новых инцидентов?
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                <button type="button" id="start_escalation" class="btn btn-primary"
                        data-dismiss="modal">Запустить</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>