<%@ page import="arm.monitoring.Issue" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'roadMap.label', default: 'RoadMap')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-roadMap" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-roadMap" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="['Дорожной карты']"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.roadMap}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.roadMap}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.roadMap}" method="POST">
        <fieldset class="form col-sm-6 create-f">
            <my:select value="${roadMap.issueId}" class="form-control" label="Замечание"
                       name="issue.id" from="${arm.monitoring.Issue.list()}"
                       optionKey="id" optionValue="name"/>

            <my:field label="Название" bean="${roadMap}" prop="name"></my:field>
            <my:datePicker label="Дата начала" bean="${roadMap}" prop="startDate" value="${new Date()}"
                           precision="day" years="${2017..2030}">

            </my:datePicker>
            <my:datePicker label="Дата окончания" bean="${roadMap}" prop="endDate" value="${new Date()}"
                           precision="day" years="${2017..2030}">

            </my:datePicker>
            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
