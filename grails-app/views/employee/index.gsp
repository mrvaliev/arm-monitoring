<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-employee" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" action="create">Cоздать нового сотрудника</g:link></li>
        <li class="pull-right">
            <div class="pagination pagination-custom">
                <g:paginate total="${issueCount ?: 0}"/>
            </div>
        </li>
    </ul>
</div>

<div id="list-employee" class="content scaffold-list" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table class="table table-bordered table-hover">
        <thead>
        <th>ФИО</th>
        <th>Цех</th>
        <th colspan="3">Действия</th>
        </thead>
        <tbody>

        <g:each in="${employeeList}" var="emp">
            <tr>
                <td>${emp.fio}</td>
                <td>${emp.department.name}</td>
                <td>
                    <g:link action="incidents" params="[empID: emp.id, date: curDate]">
                        <span title="Посмотреть инциденты" class="glyphicon glyphicon-exclamation-sign"></span>
                    </g:link>
                </td>
                <td><g:link action="edit" id="${emp.id}">
                    <span title="Изменить" class="glyphicon glyphicon-edit"></span></g:link>
                </td>
                <td>
                    <a href="#" data-toggle=confirmation
                       data-href="${request.contextPath}/department/delete/${emp.id}">
                        <span title="Удалить" class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
        </g:each>

        </tbody>
    </table>
</div>
</body>
</html>