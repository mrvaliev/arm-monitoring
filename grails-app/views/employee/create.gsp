<%@ page import="arm.monitoring.Department" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-employee" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                 default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-employee" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.employee}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.employee}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.employee}" method="POST">
        <fieldset class="form col-sm-6" style="background-color: #ebffa7">

            <my:select name="department.id" label="Цех"
                       from="${arm.monitoring.Department.list()}"
                       optionKey="id"
                       optionValue="name"/>

            <my:field bean="${employee}" prop="fio" label="ФИО"/>
            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
