<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="nav" role="navigation">
    <ul style="margin-top: 5px">
        <li><g:link class="create" controller="incident" action="create"
                    params="[empId: employee.id]">Новый инцидент</g:link></li>

        <li class="form-inline">
            <div class="form-group ">
                <label for="datepicker">Фильтр по дню</label>
                <input type="text" class="form-control" id="datepicker"
                       value="${curDay}"
                       data-provide="datepicker-inline"
                       data-date-format="mm.dd.yyyy">
                <button id="datepicker-ok-btn" class="btn btn-warning">Применить</button>
            </div>
        </li>
    </ul>

    <div class="content scaffold-list" role="main">
        <table class="table table-bordered">
            <thead>
            <th>Цех</th>
            <th>ФИО <br/> работника</th>
            <th>Выявленная проблема</th>
            <th>Заметка</th>
            <th>Дата <br/> нарушения</th>
            <th>Время <br/> выявления</th>
            <th>Состояние</th>
            <th>Время <br/> эскалации <br/> на тек. уровень</th>
            <th>Время <br/> на решение <br/> (час)</th>
            <th>Текущий <br/> уровень</th>
            <th>Создатель</th>
            </thead>
            <tbody>
            <g:each in="${incidents}" var="inc">
                <tr>
                    <td>${inc.employee.department.name}</td>
                    <td>${inc.employee.fio}</td>
                    <td>${inc.issue.name}</td>
                    <td>${inc.descr}</td>
                    <td><g:formatDate format="yyyy-MM-dd" date="${inc.issueTime}"/></td>
                    <td><g:formatDate format="yyyy-MM-dd" date="${inc.detectTime}"/></td>
                    <g:if test="${inc.solved}">
                        <td class="success">Решено</td>
                    </g:if>
                    <g:else>
                        <td class="danger">не решено</td>
                    </g:else>
                    <td>
                        <g:if test="${inc.escalation != null}">
                            <g:formatDate format="yyyy-MM-dd HH:mm" date="${inc.escalation?.dateTime}"/>
                        </g:if>
                        <g:else>
                            Нет
                        </g:else>
                    </td>
                    <td>${inc.getSolvingTime()}</td>
                    <td>${inc.getEscalation()?.getLevel()?.getLabel()}</td>
                    <td>${inc.createdBy.fio}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
</body>