<%@ page import="arm.monitoring.security.User" %>
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="${message(code: 'default.app.title')}"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>

<body style="background: beige; overflow: auto">

<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#">
                Автоматизированное рабочее места специалиста мониторингового центра НГДУ
            </a>

            <a class="nav-divider"></a>
            <a class="nav-divider"></a>
            <a class="nav-divider"></a>
            <g:if test="${userRole != null}">
                <p class="navbar-brand blue">
                    ${userRole.role.authority} : ${userRole.user.fio}
                </p>
            </g:if>
            <a class="nav-divider"></a>
            <a class="navbar-brand">${place}</a>
        </div>

        <sec:ifLoggedIn>
            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
                <ul class="nav navbar-nav navbar-right">
                    %{--<g:pageProperty name="page.nav"/>--}%
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Меню<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="${request.contextPath}/incident">Инциденты</a></li>
                            <li><a href="${request.contextPath}/employee">Сотрудники</a></li>
                            <li><a href="${request.contextPath}/document">Справочные материалы</a></li>
                            <sec:ifAllGranted roles="ADMIN">
                                <li><a href="${request.contextPath}/department">Цеха (подразделения)</a></li>
                                <li><a href="${request.contextPath}/issue">Справочник замечаний</a></li>
                                <li><a href="${request.contextPath}/issue">Дорожные карты</a></li>
                                <li><a href="${request.contextPath}/user">Пользователи</a></li>
                            </sec:ifAllGranted>
                            <li><a href="/logoff">Выход</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </sec:ifLoggedIn>

    </div>
</div>

<div class="container">
    <g:layoutBody/>
</div>


<div class="footer" role="contentinfo"></div>

<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
