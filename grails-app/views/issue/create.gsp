<%@ page import="arm.monitoring.IssueGroup" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'issue.label', default: 'Issue')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-issue" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="['Замечания']"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.issue}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.issue}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.issue}" method="POST">

        <fieldset class="form col-sm-6 create-f" style="background-color: #ebffa7">
            <fieldset class="fieldset">
                <legend>Группа и наименование</legend>
                <my:select value="${issue?.issueGroup?.id}" class="form-control"
                           name="issueGroup.id" from="${IssueGroup.list()}" optionKey="id" optionValue="name"/>
                <my:field bean="${issue}" prop="name" placeholder="Наименование"/>
            </fieldset>

            <div class="form-group">
                <fieldset class="fieldset">
                    <legend>1й уровень эскалации</legend><br/>
                    <my:field bean="${issue}" prop="email1" label="Email адреса"/>
                    <my:field bean="${issue}" placeholder="24"
                              prop="time1"
                              label="Время эскалации на след. уровень (часов):"/>
                </fieldset>
            </div>

            <div class="form-group">
                <fieldset class="fieldset">
                    <legend>2й уровень эскалации</legend><br/>
                    <my:field bean="${issue}" prop="email2" label="Email адреса"/>
                    <my:field bean="${issue}" placeholder="24"
                              prop="time2"
                              label="Время эскалации на след. уровень (часов):"/>
                </fieldset>
            </div>

            <div class="form-group">
                <fieldset class="fieldset">
                    <legend>3й уровень эскалации</legend><br/>
                    <my:field bean="${issue}" prop="email3" label="Email адреса"/>
                    <my:field bean="${issue}" placeholder="24"
                              prop="time3"
                              label="Время эскалации на след. уровень (часов):"/>
                </fieldset>
            </div>

            <div class="form-group">
                <fieldset class="fieldset">
                    <legend>4й уровень эскалации</legend><br/>
                    <my:field bean="${issue}" prop="email4" label="Email адреса"/>
                    <my:field bean="${issue}" placeholder="24"
                              prop="time4"
                              label="Время эскалации на след. уровень (часов):"/>
                </fieldset>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
