<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'issue.label', default: 'Issue')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-issue" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                            default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" action="create">Создать новое замечание</g:link></li>

        <li class="pull-right">
            <div class="pagination pagination-custom">
                <g:paginate total="${issueCount ?: 0}"/>
            </div>
        </li>
    </ul>
</div>

<div id="list-issue" class="content scaffold-list" role="main">

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="alert-popup" role="alert"></div>
    <table class="table table-bordered table-hover" title="Справочник замечаний">
        <colgroup>
            <col style="width: 1%">
            <col style="width: 10%">
            <col style="width: 15%">
        </colgroup>
        <thead>
        <tr>
            <th rowspan="2">№</th>
            <th rowspan="2">Группа</th>
            <th rowspan="2">Название</th>
            <th colspan="2">1й уровень</th>
            <th colspan="2">2й уровень</th>
            <th colspan="2">3й уровень</th>
            <th colspan="2">4й уровень</th>
            <th colspan="3">Название проекта, ДК</th>
            <th rowspan="2">Действия</th>
        </tr>
        <tr>
            <th>email</th>
            <th>время</th>
            <th>email</th>
            <th>время</th>
            <th>email</th>
            <th>время</th>
            <th>email</th>
            <th>время</th>
            <th>Наименование</th>
            <th>Дата начала</th>
            <th>Дата окончания</th>
        </tr>
        </thead>
        <tbody>

        <g:each in="${issueList}" var="i">
            <tr>
                <td>${i.id}</td>
                <td>${i.issueGroup.name}</td>
                <td>${i.name}</td>
                <td>${i.email1?.replace(";", ", ")}</td>
                <td>${i.time1}</td>
                <td>${i.email2?.replace(";", ", ")}</td>
                <td>${i.time2}</td>
                <td>${i.email3?.replace(";", ", ")}</td>
                <td>${i.time3}</td>
                <td>${i.email4?.replace(";", ", ")}</td>
                <td>${i.time4}</td>
                <td>${i.roadMap?.name}</td>
                <td>${i.roadMap?.startDate}</td>
                <td>${i.roadMap?.endDate}</td>

                <td class="actions-cell"><g:link action="edit" id="${i.id}">
                    <span title="Изменить"
                          class="glyphicon glyphicon-edit"></span></g:link>

                    <a href="${request.contextPath}/roadMap/create?issue=${i.id}" title="Добавить дорожную карту"
                        <span class="glyphicon glyphicon-tasks"></span>
                    </a>

                    <a href="#" data-toggle=confirmation
                       data-href="${request.contextPath}/department/delete/${i.id}">
                        <span title="Удалить" class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>

</div>
</body>
</html>