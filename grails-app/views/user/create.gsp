<%@ page import="arm.monitoring.security.UserRole" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                             default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-user" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="['Пользователя']"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.user}" method="POST">
        <fieldset class="form col-sm-6 create-f" style="background-color: #ebffa7">
            <my:field label="ФИО" bean="${user}" prop="fio"></my:field>
            <my:field label="Email" bean="${user}" prop="username"></my:field>
            <my:field label="password" bean="${user}" prop="password"></my:field>
            <my:select value="${user?.userRoles*.role?.id}" class="form-control"
                       name="roles" from="${arm.monitoring.security.Role.list()}" optionKey="id"
                       optionValue="name"></my:select>
            <label for="enabled">
                Активен
            </label>
            <g:checkBox name="enabled" value="${user.enabled}"></g:checkBox>
            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
