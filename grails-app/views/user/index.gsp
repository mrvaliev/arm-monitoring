<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li>
            <g:link class="create" action="create">
                <g:message code="default.new.label" args="['пользователя']"/>
            </g:link>
        </li>

        <div class="pagination pagination-custom pull-right">
            <g:paginate total="${userCount ?: 0}"/>
        </div>
    </ul>
</div>

<div id="list-user" class="content scaffold-list" role="main">
    <h1>Пользователи программы</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="alert-popup" role="alert"></div>
    <table class="table table-bordered table-hover">
        <thead>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th>Статус</th>
        <th>Действия</th>
        </thead>
        <tbody>
        <g:each var="user" in="${userList}">
            <tr>
                <td>${user.fio}</td>
                <td>${user.username}</td>
                <td>
                    <g:each var="role" in="${user.getAuthorities()}">
                        ${role.authority}
                    </g:each>
                </td>

                <g:if test="${user.enabled}">
                    <td class="success">
                        Включен
                    </td>
                </g:if>
                <g:else>
                    <td class="danger">
                        Отключен
                    </td>
                </g:else>
                <td class="actions-cell"><g:link action="edit" id="${user.id}">
                    <span title="Изменить" class="glyphicon glyphicon-edit"></span></g:link>

                    <a href="#" data-toggle=confirmation data-href="${request.contextPath}/user/delete/${user.id}">
                        <span title="Удалить" class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>