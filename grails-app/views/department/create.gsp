<%@ page import="arm.monitoring.DepartmentGroup" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'department.label', default: 'Department')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-department" class="content scaffold-create" role="main">

    <h1><g:message code="default.create.label" args="${['Цех']}"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.department}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.department}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.department}" method="POST">
        <fieldset class="form col-sm-6" style="background-color: #ebffa7">

            <my:select name="group.id" label="Группа"
                       from="${DepartmentGroup.list()}"
                       optionKey="id"
                       optionValue="name"/>

            <my:field bean="${department}" prop="name" label="Название цеха"/>
            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
        </fieldset>
    </g:form>
</div>
</body>
</html>
