<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'department.label', default: 'Department')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<g:render template="/shared/mainMenu"></g:render>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" action="create">Добавить цех</g:link></li>
        <li class="pull-right">
            <div class="pagination">
                <g:paginate total="${departmentCount ?: 0}"/>
            </div>
        </li>
    </ul>
</div>

<div id="list-department" class="content scaffold-list" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="alert-popup" role="alert"></div>
    <table class="table table-bordered table-hover">
        <thead>
        <th>№</th>
        <th>Группа</th>
        <th>Название</th>
        <th colspan="2">Действия</th>
        </thead>
        <tbody>

        <g:each in="${departmentList}" var="dep">
            <tr>
                <td>${dep.id}</td>
                <td>${dep.group?.name}</td>
                <td>${dep.name}</td>
                <td><g:link action="edit" id="${dep.id}"><span title="Изменить"
                                                               class="glyphicon glyphicon-edit"></span></g:link>
                </td>
                <td>
                    <a href="#" data-toggle=confirmation
                       data-href="${request.contextPath}/department/delete/${dep.id}">
                        <span title="Удалить" class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
        </g:each>

        </tbody>
    </table>
</div>
</body>
</html>