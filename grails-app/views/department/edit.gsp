<%@ page import="arm.monitoring.DepartmentGroup" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'department.label', default: 'Department')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-department" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-department" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="ЦЕХ" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.department}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.department}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.department}" method="PUT">
                <g:hiddenField name="version" value="${this.department?.version}" />
                <fieldset class="form col-sm-6" style="background-color: #ebffa7">

                    <my:select name="group.id" label="Группа"
                               from="${arm.monitoring.DepartmentGroup.list()}"
                               optionKey="id"
                               optionValue="name"/>

                    <my:field bean="${department}" prop="name" label="Название цеха"/>
                    <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
