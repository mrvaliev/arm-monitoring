package arm.monitoring

class ArmTagLib {

//    static defaultEncodeAs = [taglib: 'html']
    static namespace = 'my'
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]


    def field = { attr ->
        out << '<div class=\"form-group\">'
        if (attr.label) {
            out << '<label>'
            out << attr.label
            out << '</label>'
        }
        attr.type = attr.type ?: "text"
        attr.class = 'form-control'
        attr.name = attr.prop;
        attr.value = attr.bean."$attr.prop"
        out << g.field(attr)
        out << '</div>'
    }


    def datePicker = { attr ->
        out << '<div class=\"form-group\">'
        attr.name = attr.prop ?: attr.name;
        if (attr.label) {
            out << '<label>'
            out << attr.label
            out << '</label>'
            out << '<br/>'
        }
        out << g.datePicker(attr)
        out << '</div>'
    }

    def select = { attr ->
        out << '<div class=\"form-group\">'
        if (attr.label) {
            out << '<label>'
            out << attr.label
            out << '</label>'
        }
        attr.class = 'form-control'
        out << g.select(attr)
        out << '</div>'
    }


}
