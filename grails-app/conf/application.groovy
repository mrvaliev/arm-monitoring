grails.gorm.default.mapping = {
    cache true
    id generator: 'identity'
}

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'arm.monitoring.security.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'arm.monitoring.security.UserRole'
grails.plugin.springsecurity.authority.className = 'arm.monitoring.security.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['isAuthenticated()']],
        [pattern: '/error', access: ['isAuthenticated()']],
        [pattern: '/index', access: ['isAuthenticated()']],
        [pattern: '/index.gsp', access: ['isAuthenticated()']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/incident/*', access: ['permitAll']], // ADMIN
        [pattern: '/department/*', access: ['permitAll']], // ADMIN
        [pattern: '/issue/*', access: ['isAuthenticated()']], // ADMIN
        [pattern: '/user/*', access: ['permitAll']], // ADMIN
        [pattern: '/roadmap/*', access: ['isAuthenticated()']], // ADMIN
        [pattern: '/roadmap/create/*', access: ['permitAll']], // ADMIN
        [pattern: '/document/*', access: ['permitAll']], // ADMIN
        [pattern: '/employee/*', access: ['permitAll']] // ADMIN
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

