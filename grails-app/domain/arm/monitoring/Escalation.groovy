package arm.monitoring

class Escalation {

    EscalationLevel level;

    // описание решения на текущем уровне
    String levelNote;

    Date dateTime;

    Escalation parent

    static belongsTo = [incident: Incident]

    static mapping = {
        parent column: "parent_id"

    }

    static constraints = {
        levelNote nullable: true
        parent nullable: true
    }
}
