package arm.monitoring

/**
 * Вид цеха
 */
class DepartmentGroup {

    String name;

    static constraints = {
        name maxSize: 255
        name minSize: 1
    }
}
