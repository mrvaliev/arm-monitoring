package arm.monitoring

import arm.monitoring.security.User

/**
 * инцидент - определенный факт нарушения из перечня субъектом которого является работник цеха
 */
class Incident {

    // тип проблемы
    Issue issue;

    Boolean solved = false
    // описание проблемы
    String descr;
    // дата нарушения
    Date issueTime
    // дата выявляения нарушеиня
    Date detectTime;
    // дата экскалации на текущий уровень
    Date levelTime;
    // описание решения проблемы
    String solvingDescr
    // текущий уровень эскалации
    Escalation escalation;

    Employee employee;

    User createdBy;
    User modifiedBy;

    Integer getSolvingTime() {
        if (escalation) {
            EscalationLevel level = escalation.getLevel();
            switch (level) {
                case EscalationLevel.FIRST: return issue.time1
                case EscalationLevel.SECOND: return issue.time2
                case EscalationLevel.THIRD: return issue.time3
                case EscalationLevel.FOURTH: return issue.time4
            }
        }
        return null;
    }

    static belongsTo = [employee: Employee]

    static constraints = {
        descr nullable: true
        escalation nullable: true
        solvingDescr nullable: true
        levelTime nullable: true
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
