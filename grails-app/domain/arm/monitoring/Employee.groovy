package arm.monitoring

class Employee {

    Department department

    String fio

    static constraints = {
        fio maxSize: 255
        fio minSize: 3
    }

    static hasMany = [incidents: Incident]

}
