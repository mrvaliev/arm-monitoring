package arm.monitoring.security

import arm.monitoring.Department
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String username
    String password
    String fio
    boolean enabled = true
    boolean accountLocked
    boolean accountExpired
    boolean passwordExpired

    // цех
    Department department;

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    static hasMany = [userRoles: UserRole]

    static mappedBy = [userRoles: 'user']

    static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        department nullable: true
    }

    static mapping = {
        password column: '`password`'
        table 'security_user'
        userRoles cascade: 'all-delete-orphan'
    }
}
