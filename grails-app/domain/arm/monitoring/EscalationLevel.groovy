package arm.monitoring

/**
 * Created by aivaliev on 23.05.2017.
 */
enum EscalationLevel {
    FIRST, SECOND, THIRD, FOURTH

    def getLabel() {
        switch (this) {
            case FIRST: return "1й уровень"
            case SECOND: return "2й уровень"
            case THIRD: return "3й уровень"
            case FOURTH: return "4й уровень"
        }
    }
}