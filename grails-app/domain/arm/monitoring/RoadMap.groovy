package arm.monitoring

class RoadMap {

    Date startDate;
    Date endDate;
    String name;

    static belongsTo = [issue: Issue]

    static constraints = {
    }
}
