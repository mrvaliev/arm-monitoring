package arm.monitoring

class Issue {

    IssueGroup issueGroup;
    String name;

    int time1 = 1;
    int time2 = 1;
    int time3 = 1;
    int time4 = 1;

    String email1;
    String email2;
    String email3;
    String email4;

    RoadMap roadMap;

    static constraints = {
        email1 nullable: true
        email2 nullable: true
        email3 nullable: true
        email4 nullable: true
        roadMap nullable: true
    }
}
