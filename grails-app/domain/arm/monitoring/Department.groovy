package arm.monitoring
/**
 * Цех
 */
class Department {

    String name;

    DepartmentGroup group;

    static constraints = {
        name maxSize: 255
        name minSize: 1
    }
}
