// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {

    (function ($) {

        function deleteObject() {
            var elem = $(this.context);
            var href = elem.attr('data-href');
            $.ajax({
                    url: href,
                    type: "DELETE",
                    success: function (data) {
                        elem.parents("tr").detach();
                        $(".alert-popup").addClass("green").text("Удаление успешно");
                    },
                    error: function (data) {
                        $(".alert-popup").addClass("red").text("Удаление невозможно (вероятно есть связанные объекты)");
                    }
                }
            )
        }

        $(document).ajaxStart(function () {
            $('#spinner').fadeIn();
        }).ajaxStop(function () {
            $('#spinner').fadeOut();
        });
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: deleteObject,
            title: "Вы уверены?",
            btnOkLabel: "Да",
            btnCancelLabel: "Нет",
            // other options
        });


        $("#start_escalation").click(function () {
            $.post({
                url: "/incident/start-escalation",
                success: function (data) {
                    window.alert("Запущена")
                },
                error: function () {
                    window.alert("Произошла ошибка!")
                }
            })
        });

        function addQueryParam(name, value) {
            if (document.location.href.indexOf('?') != -1) {
                var url = document.location.href + "&" + name;
            } else {
                var url = document.location.href + "?" + name;
            }
            document.location = url + "=" + value;
        }


        function updateIssueOptions(data) {
            var $el = $("#issueSelect")
            $el.empty();
            data.forEach(function (object, index) {
                var $option = $("<option></option>").attr("value", object.id).text(object.name);
                $el.append($option);
            });
        }


        $("#issueGroupSelect").on("change", function () {
            var val = $(this).val();
            if (!val || val.length === 0) {
                var $el = $("#issueSelect")
                $el.empty();
                return;
            }
            $.getJSON("/issue/list?group=" + val, null, function (data) {
                updateIssueOptions(data);
            })
        });

        $("#datepicker").datepicker({
            autoclose: true,
            language: "ru",
            format: 'dd.mm.yyyy',
        });

        $('#datepicker-ok-btn').on("click", function () {
            var date = $("#datepicker").datepicker("getDate");
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            var dateStr = 'date=' + day + '.' + month + '.' + year;
            var newSearch = window.location.search.replace(/date=.*/, dateStr);
            window.location.search = newSearch;
        });


    })(jQuery);
}