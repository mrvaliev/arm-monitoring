package arm.monitoring

import arm.monitoring.security.Role

class BootStrap {

    InitDataService initDataService;

    def init = { servletContext ->
        if (Role.list().isEmpty()) {
            initDataService.fillDatabase()
            initDataService.fillIssues()
            initDataService.createTestIncidents(20);
        }
    }
    def destroy = {
    }
}
