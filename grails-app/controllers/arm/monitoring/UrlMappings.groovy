package arm.monitoring

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(redirect: "/incident/index")
//        get "/incident/report"(action: 'report')
        post "/incident/report"(controller: 'incident', action: 'reportSave')
        post "/incident/start-escalation"(controller: 'incident', action: 'startEscalation')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
