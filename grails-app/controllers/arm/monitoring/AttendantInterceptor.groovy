package arm.monitoring

import arm.monitoring.security.User
import arm.monitoring.security.UserRole
import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import groovy.transform.CompileStatic
import org.springframework.security.core.userdetails.UserDetails

@CompileStatic
@Transactional
class AttendantInterceptor {


    AttendantInterceptor() {
        matchAll()
    }

    SpringSecurityService springSecurityService;


    boolean before() { true }

    boolean after() {
        if (springSecurityService.isLoggedIn()) {
            UserDetails auth = (UserDetails) springSecurityService.getPrincipal()
            String username = auth.getUsername()
            UserRole user = UserRole.find("from UserRole where user.username=:name", [name: username])
            if (!model)
                model = [:]
            model.put("userRole", user);
        }
        true
    }

    void afterView() {
        // no-op
    }
}
