package arm.monitoring

import arm.monitoring.security.User
import grails.transaction.Transactional

import java.text.SimpleDateFormat
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class IncidentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    public static String SHORT_DATE_PATTERN = "dd.MM.yyyy";

    ExecutorService executor = Executors.newSingleThreadExecutor()

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.YYYY");

    EscalationService escalationService

    UserInfoService userInfoService;

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = "id"
        def incidents = Incident.list(params);
        def today = new Date().format(SHORT_DATE_PATTERN);
        render(view: 'index', model: [incidentList: incidents, curDate: today, incidentCount: Incident.count()])
    }

    def show(Incident incident) {
        respond incident
    }

    def create() {
        def employee
        if (params.empId) {
            employee = Employee.findById(params.empId)
        }
        render(view: 'create', model: [emp: employee])
        respond new Incident(params)
    }

    def report(Incident incident) {
        respond(incident)
    }


    def startEscalation() {
        executor.execute({
            escalationService.initalEscalation()
        })
        render "OK"
    }

    def reportSave(Incident incident) {
        incident.save flush: true
        if (incident.escalation) {
            incident.escalation.levelNote = incident.solvingDescr
        }
        if (incident.solved)
            flash.message = "Инцидент $incident.id закрыт"
        redirect(action: 'index')
    }

    @Transactional
    def save(Incident incident) {

        if (incident == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        User user = userInfoService.getCurrentUser();
        if (user) {
            incident.setModifiedBy(user)
            incident.setCreatedBy(user)
        }

        if (incident.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond incident.errors, view: 'create'
            return
        }
        incident.save flush: true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: ["Инцидент", incident.id])
                redirect action: 'index'
            }
            '*' { respond incident, [status: CREATED] }
        }
    }

    def edit(Incident incident) {
        respond incident
    }

    @Transactional
    def update(Incident incident) {
        if (incident == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (incident.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond incident.errors, view: 'edit'
            return
        }

        incident.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'incident.label', default: 'Incident'), incident.id])
                redirect incident
            }
            '*' { respond incident, [status: OK] }
        }
    }

    @Transactional
    def delete(Incident incident) {

        if (incident == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        incident.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'incident.label', default: 'Incident'), incident.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'incident.label', default: 'Incident'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
