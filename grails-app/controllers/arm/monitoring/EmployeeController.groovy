package arm.monitoring

import grails.transaction.Transactional

import java.text.SimpleDateFormat
import arm.monitoring.Incident

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class EmployeeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    SimpleDateFormat dateFormat = new SimpleDateFormat(IncidentController.SHORT_DATE_PATTERN)

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def list = Employee.list(params)
        def curDate = new Date();
        def dateString = curDate.format(IncidentController.SHORT_DATE_PATTERN)
        render(view: 'index', model: [employeeList: list, curDate: dateString, employeeCount: Employee.count()])
    }

    def show(Employee employee) {
        respond employee
    }

    def create() {
        respond new Employee(params)
    }

    def incidents() {
        def employee = Employee.findById(params.empID)
        Date date = dateFormat.parse(params.date)
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        def tomorrow = calendar.getTime();
//        def list = Incident.findAllByEmployeeAndIssueTimeGreaterThan(employee, date);
        def list = Incident.createCriteria().list {
            and {
                between('issueTime', date, tomorrow)
                eq('employee.id', employee.id)
            }
        }
//        def query = Incident.where {
//            (issueTime < date && issueTime < tomorrow) && this.employee.id == employee.id
//        }
//        def list = criteria.list {};
        render(view: 'incidents', model: [incidents: list, employee: employee, curDay: params.date])
    }

    @Transactional
    def save(Employee employee) {
        if (employee == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (employee.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond employee.errors, view: 'create'
            return
        }

        employee.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'employee.label', default: 'Employee'), employee.id])
                redirect action: 'index'
            }
            '*' { respond employee, [status: CREATED] }
        }
    }

    def edit(Employee employee) {
        respond employee
    }

    @Transactional
    def update(Employee employee) {
        if (employee == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (employee.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond employee.errors, view: 'edit'
            return
        }

        employee.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'employee.label', default: 'Employee'), employee.id])
                redirect action: 'index'
            }
            '*' { respond employee, [status: OK] }
        }
    }

    @Transactional
    def delete(Employee employee) {

        if (employee == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        employee.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'employee.label', default: 'Employee'), employee.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
