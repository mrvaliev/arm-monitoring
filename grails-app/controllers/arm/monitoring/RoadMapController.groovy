package arm.monitoring

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RoadMapController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RoadMap.list(params), model: [roadMapCount: RoadMap.count()]
    }

    def show(RoadMap roadMap) {
        respond roadMap
    }

    def create(Integer issue) {
        def roadMap=new RoadMap();
        roadMap.issue=Issue.get(issue)
        render view: "create", model: [roadMap: roadMap ]
    }

    @Transactional
    def save(RoadMap roadMap) {
        if (roadMap == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (roadMap.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond roadMap.errors, view: 'create'
            return
        }

        roadMap.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'roadMap.label', default: 'RoadMap'), roadMap.id])
                redirect roadMap
            }
            '*' { respond roadMap, [status: CREATED] }
        }
    }

    def edit(RoadMap roadMap) {
        respond roadMap
    }

    @Transactional
    def update(RoadMap roadMap) {
        if (roadMap == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (roadMap.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond roadMap.errors, view: 'edit'
            return
        }

        roadMap.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'roadMap.label', default: 'RoadMap'), roadMap.id])
                redirect roadMap
            }
            '*' { respond roadMap, [status: OK] }
        }
    }

    @Transactional
    def delete(RoadMap roadMap) {

        if (roadMap == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        roadMap.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'roadMap.label', default: 'RoadMap'), roadMap.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'roadMap.label', default: 'RoadMap'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
